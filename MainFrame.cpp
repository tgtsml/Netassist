#include "MainFrame.h"
#include <QTabWidget>
#include <QDebug>
#include <QGridLayout>
#include <QPushButton>
#include <QMenuBar>
#include <QMenu>
#include "SerialPort.h"
#include "TCPServer.h"
#include "TCPClient.h"
#include "UDPConnection.h"
#include <QLabel>
#include <QDialog>

MainFrame::MainFrame(QWidget *parent) : QWidget(parent)
{
    m_tabWgt = new QTabWidget(this);
    m_tabWgt->setTabsClosable(true);
    m_tabWgt->setMovable(true);

    QMenuBar *menuBar = createMenuBar();

    QGridLayout *layoutSurface = new QGridLayout(this);
    layoutSurface->setMargin(0);
    layoutSurface->setSpacing(0);
    layoutSurface->addWidget(menuBar, 0, 0);
    layoutSurface->addWidget(m_tabWgt, 2, 0);

    connect(m_tabWgt, &QTabWidget::tabCloseRequested, this, [=](int index){
        m_tabWgt->widget(index)->deleteLater();
        m_tabWgt->removeTab(index);
    });

    createSerialPortWidget();
}

QMenuBar* MainFrame::createMenuBar()
{
    QMenu *menuNew = new QMenu("New", this);
    menuNew->addAction("New Serial Port", this, &MainFrame::createSerialPortWidget);
    menuNew->addAction("New TCP Server", this, &MainFrame::createTCPServerWidget);
    menuNew->addAction("New TCP Client", this, &MainFrame::createTCPClientWidget);
    menuNew->addAction("New UDP Connection", this, &MainFrame::createUDPConnectionWidget);
    menuNew->addSeparator();
    menuNew->addAction("Quit", this, &MainFrame::close);

    QMenu *menuAbout = new QMenu("About", this);
    menuAbout->addAction("About Netassist", this, &MainFrame::aboutXTTool);

    QMenuBar *menuBar = new QMenuBar(this);
    menuBar->addMenu(menuNew);
    menuBar->addMenu(menuAbout);
    return menuBar;
}

QString MainFrame::getNewWidgetName()
{
    static int index = 1;
    return QString("Untitled-%1").arg(index++);
}

void MainFrame::createSerialPortWidget()
{
    SerialPort *tmpWgt = new SerialPort(this);
    m_tabWgt->addTab(tmpWgt, getNewWidgetName());
    m_tabWgt->setCurrentWidget(tmpWgt);

    connect(tmpWgt, &SerialPort::signalUpdateTitle, this, [=](QString text){
        QWidget *wgt = qobject_cast<QWidget*>(sender());
        m_tabWgt->tabBar()->setTabText(m_tabWgt->indexOf(wgt), text);
    });
}

void MainFrame::createTCPServerWidget()
{
    TCPServer *tmpWgt = new TCPServer(this);
    m_tabWgt->addTab(tmpWgt, getNewWidgetName());
    m_tabWgt->setCurrentWidget(tmpWgt);

    connect(tmpWgt, &TCPServer::signalUpdateTitle, this, [=](QString text){
        QWidget *wgt = qobject_cast<QWidget*>(sender());
        m_tabWgt->tabBar()->setTabText(m_tabWgt->indexOf(wgt), text);
    });
}

void MainFrame::createTCPClientWidget()
{
    TCPClient *tmpWgt = new TCPClient(this);
    m_tabWgt->addTab(tmpWgt, getNewWidgetName());
    m_tabWgt->setCurrentWidget(tmpWgt);

    connect(tmpWgt, &TCPClient::signalUpdateTitle, this, [=](QString text){
        QWidget *wgt = qobject_cast<QWidget*>(sender());
        m_tabWgt->tabBar()->setTabText(m_tabWgt->indexOf(wgt), text);
    });
}

void MainFrame::createUDPConnectionWidget()
{
    UDPConnection *tmpWgt = new UDPConnection(this);
    m_tabWgt->addTab(tmpWgt, getNewWidgetName());
    m_tabWgt->setCurrentWidget(tmpWgt);

    connect(tmpWgt, &UDPConnection::signalUpdateTitle, this, [=](QString text){
        QWidget *wgt = qobject_cast<QWidget*>(sender());
        m_tabWgt->tabBar()->setTabText(m_tabWgt->indexOf(wgt), text);
    });
}

void MainFrame::aboutXTTool()
{
    QLabel lb;
    lb.setWordWrap(true);
    lb.setText("Netassist V1.2.2\r\n\r\nA tool contains Serial Port, TCP Server, TCP Client, UDP Socket and so on.\r\n\r\nCopyright tgtsml. All rights reserved.");

    QDialog *dlg = new QDialog(this);
    dlg->setWindowTitle("About Netassist");
    dlg->setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
    dlg->setFixedSize(400, 200);

    QGridLayout *gl = new QGridLayout(dlg);
    gl->addWidget(&lb);
    dlg->exec();

    dlg->deleteLater();
    gl->deleteLater();
}
