#ifndef MAINFRAME_H
#define MAINFRAME_H

#include <QWidget>

class QTabWidget;
class QMenuBar;

class MainFrame : public QWidget
{
    Q_OBJECT
public:
    explicit MainFrame(QWidget *parent = nullptr);

private:
    QMenuBar* createMenuBar();

    QString getNewWidgetName();

    void createSerialPortWidget();
    void createTCPServerWidget();
    void createTCPClientWidget();
    void createUDPConnectionWidget();

    void aboutXTTool();

private:
    QTabWidget *m_tabWgt;

};

#endif // MAINFRAME_H
