#include <QApplication>
#include "MainFrame.h"
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainFrame mf;
    mf.setWindowIcon(QIcon(":/image/logo.png"));
    mf.setWindowTitle("Netassist V1.2.2 @tgtsml");
    mf.resize(800, 600);
    mf.show();

    return a.exec();
}
