# 简介
Netassist 是一款基于Qt5开发的调试工具,包含串口调试及网络调试

# 计划任务
- [x] UI设计
- [x] 串口调试
- [x] TCP调试，包含TCP Server和TCP Client
- [x] UDP调试

# 软件截图
![](./image/app.png)