#ifndef UDPCONNECTION_H
#define UDPCONNECTION_H

#include <QWidget>
#include <QComboBox>

class QTextEdit;
class QUdpSocket;
class QPushButton;
class QTimer;
class QSpinBox;
class QGroupBox;

class UDPConnection : public QWidget
{
    Q_OBJECT
public:
    explicit UDPConnection(QWidget *parent = nullptr);
    ~UDPConnection();

signals:
    void signalUpdateTitle(QString text);

private:
    void initWidget();
    QStringList getAvailableComs();
    void updateControlState();
    void addLog(QString log);

    void slot_netOpen();
    void slot_readyRead();
    void slot_netSend();

    void slot_hexRecieve(int state);
    void slot_showTime(int state);
    void slot_wordWrap(int state);
    void slot_hexSend(int state);
    void slot_autoSend(int state);
    void slot_bindSendAndRecieve(int state);

    struct{
        bool showTime;
        bool showHex;
        bool sendHex;
        bool bindSendRecv;
    }m_netSettings;
    QTextEdit *m_textSend, *m_textRecv;
    QUdpSocket *m_udpSocket;
    QPushButton *m_btnOpen, *m_btnSend;
    QTimer *m_autoSendTimer;
    QSpinBox *m_spinBoxSendMs;

    QLineEdit *m_ctrlTargetIP, *m_ctrTargetPort;
    QLineEdit *m_ctrlTargetSendIP, *m_ctrTargetSendPort;
    QGroupBox *group_targetSendNet;
};

#endif // UDPCONNECTION_H
