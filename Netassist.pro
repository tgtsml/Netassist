QT = core gui widgets serialport network

TARGET = Netassist

DESTDIR = $$PWD/bin

SOURCES += \
    SerialPort.cpp \
    TCPClient.cpp \
    TCPServer.cpp \
    UDPConnection.cpp \
    main.cpp \
    MainFrame.cpp

HEADERS += \
    MainFrame.h \
    SerialPort.h \
    TCPClient.h \
    TCPServer.h \
    UDPConnection.h

RESOURCES += \
    res.qrc

contains(QT_ARCH, i386) {
    DESTDIR = $$PWD/bin/x86
}else {
    DESTDIR = $$PWD/bin/x86_64
}
